<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
#for validation
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/admin/create/publicuser', function (Request $request) {
    $validation = Validator::make($request->all(), [
        'first_name'=>'required|max:191',
        'last_name'=>'required|max:191',
        'middle_name'=>'required|max:191',
    ]);
    if($validation->fails()) {
        return response()->json([
            'status'=>400,
            'error'=>$validation->messages(),
        ]);
    } else {
        $publicuser = User::create([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'middle_name'=>$request->middle_name,
        ]);

        if($publicuser) {
            return response()->json([
                'publicuser'=> $publicuser,
                'status'=>200,
                'message'=>'User Successfully created'
            ]);
        } else {
            return response()->json([
                'status'=>400,
                'message'=>'User error'
            ]);
        }
    }
});

//for get all public user
Route::get('/admin/allpublicuser', function () {
    return User::all();
});
